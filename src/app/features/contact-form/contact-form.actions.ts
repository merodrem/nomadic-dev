import { createAction, props } from '@ngrx/store';
import { ContactForm } from './contact-form.model';

export const actionContactFormUpdate = createAction(
  '[Form] Update',
  props<{ contactform: ContactForm }>()
);

export const actionContactFormReset = createAction('[Form] Reset');
