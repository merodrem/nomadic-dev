import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { Validators, FormBuilder } from '@angular/forms';
import { Store, select, State } from '@ngrx/store';
import { filter, finalize, take, tap } from 'rxjs/operators';
import { TranslateService } from '@ngx-translate/core';
import { Observable } from 'rxjs';

import {
  ROUTE_ANIMATIONS_ELEMENTS,
  NotificationService
} from '../../../core/core.module';

import { actionContactFormReset, actionContactFormUpdate } from '../contact-form.actions';
import { AppState, selectContactFormState } from '../../../core/core.state';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { FORMSPREE_ENDPOINT } from '../../../../assets/data/conf';
import { ContactForm } from '../contact-form.model';

@Component({
  selector: 'nomadicdev-contact-form',
  templateUrl: './contact-form.component.html',
  styleUrls: ['./contact-form.component.scss'],
})
export class ContactFormComponent implements OnInit {
  routeAnimationsElements = ROUTE_ANIMATIONS_ELEMENTS;

  form = this.fb.group({
    autosave: false,
    name: ['', [Validators.required]],
    email: ['', [Validators.required, Validators.email]],
    subject: ['', [Validators.required, Validators.maxLength(200)]],
    org: ['', [Validators.maxLength(100)]],
    message: ['', [Validators.required, Validators.maxLength(2000)]],
  });

  formValueChanges$: Observable<ContactForm> | undefined;

  formSubmitting = false;

  constructor(
    private fb: FormBuilder,
    private store: Store<AppState>,
    private http: HttpClient,
    private translate: TranslateService,
    private notificationService: NotificationService
  ) { }

  ngOnInit() {
    this.formValueChanges$ = this.form.valueChanges.pipe(
      filter((form: ContactForm) => form.autosave),
      tap((updatedForm) => this.update(updatedForm))
    );
    this.store
      .pipe(select(selectContactFormState), take(1))
      .subscribe((form) => this.form.patchValue(form.contactform));
  }

  update(contactform: ContactForm) {
    this.store.dispatch(actionContactFormUpdate({ contactform }));
  }

  save() {
    this.store.dispatch(actionContactFormUpdate({ contactform: this.form.value }));
  }

  submit() {
    this.formSubmitting = true;
    if (this.form.valid) {
      const email = this.form.value;
      const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
      const message = `Subject: ${email.subject}\n${email.org ? `Org:${email.org}\n` : ''}${email.message}`;
      this.http.post(FORMSPREE_ENDPOINT,
        { name: email.name, replyto: email.email, message: message },
        { 'headers': headers })
        .pipe(
          take(1),
          finalize(() => this.formSubmitting = false)
        ).subscribe(
          response => {
            this.notificationService.success(this.translate.instant('nomadicdev.contact.emailSent'));
          },
          error => this.notificationService.warn(this.translate.instant(error))
        );
    }
  }

  reset() {

    this.form.reset();
    this.store.dispatch(actionContactFormReset());
  }
}
