import { actionContactFormReset, actionContactFormUpdate } from './contact-form.actions';
import { Action, createReducer, on } from '@ngrx/store';
import { ContactForm, ContactFormState } from './contact-form.model';

export const initialState: ContactFormState = {
  contactform: {} as ContactForm
};

const reducer = createReducer(
  initialState,
  on(actionContactFormUpdate, (state, { contactform }) => ({
    ...state,
    contactform: { ...contactform }
  })),
  on(actionContactFormReset, () => initialState)
);

export function formReducer(state: ContactFormState | undefined, action: Action) {
  return reducer(state, action);
}
