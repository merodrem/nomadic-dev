import { createSelector } from '@ngrx/store';
import { selectContactFormState } from '../../core/core.state';
import { ContactFormState } from './contact-form.model';


export const selectContactForm = createSelector(
  selectContactFormState,
    (state: ContactFormState) => state.contactform
  );
  