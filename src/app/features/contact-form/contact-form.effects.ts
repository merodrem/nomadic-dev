import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { tap } from 'rxjs/operators';

import { LocalStorageService } from '../../core/core.module';

import { actionContactFormReset, actionContactFormUpdate} from './contact-form.actions';
import { initialState } from './contact-form.reducer';

export const FORM_KEY = 'CONTACTFORM';

@Injectable()
export class ContactFormEffects {
  persistForm = createEffect(
    () =>
      this.actions$.pipe(
        ofType(actionContactFormUpdate),
        tap((action) =>
          this.localStorageService.setItem(FORM_KEY, { contactform: action.contactform })
        )
      ),
    { dispatch: false }
  );
  resetForm = createEffect(
    () =>
      this.actions$.pipe(
        ofType(actionContactFormReset),
        tap((action) =>
          this.localStorageService.setItem(FORM_KEY, { contactform: initialState })
        )
      ),
    { dispatch: false }
  );
  constructor(
    private actions$: Actions,
    private localStorageService: LocalStorageService
  ) {}
}
