import { Component, OnInit,  HostListener } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { TranslateService } from '@ngx-translate/core';

import { ROUTE_ANIMATIONS_ELEMENTS } from '../core/core.module';
import { ImageDialog } from '../shared/dialogs/image.dialog';

interface Project {
  name: string,
  description: string,
  techs: string[],
  url?: string,
  image?: string,
  play?: string,
  github?: string,
}
interface Experience {
  company: string,
  description: string,
  duration: string,
  techs: string[],
}

@Component({
  selector: 'nomadicdev-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit {
  routeAnimationsElements = ROUTE_ANIMATIONS_ELEMENTS;
  releaseButler = 'assets/release-butler.png';

  get myAge() {
    return Math.floor((Date.now() - new Date("1993-05-09T00:00:00.000Z").getTime()) / 1000 / 60 / 60 / 24 / 365)
  }

  get epflLink() {
    if (['en', 'fr'].includes(this._translate.currentLang))
      return `https://epfl.ch/${this._translate.currentLang}/`
    else
      return 'https://epfl.ch/';
  }

  REVIEWS = [
    {
      comment: "nomadicdev.reviews.umanweb",
      author: "Nadège Pricat @Umanmap",
    },
    {
      comment: "nomadicdev.reviews.amicolab",
      author: "Laurent Brunier @Amicolab",
    },
    {
      comment: "nomadicdev.reviews.2r",
      author: "Romain Rafecas @2R-retailing",
    },
    {
      comment: "nomadicdev.reviews.ben",
      author: "Ben Ayad @Upwork",
    },
  ];

  PROJECTS: Project[] = [
    {
      name: "Umanweb",
      image: "assets/img/umanweb.png",
      description: "nomadicdev.portfolio.umanweb.description",
      url: "https://www.umanweb.com",
      techs: ['Angular', 'Python', 'PostgreSQL', 'graphQL']
    },
    {
      name: "Sami",
      image: "assets/img/sami.png",
      description: "nomadicdev.portfolio.sami.description",
      url: "https://www.samicolab.com",
      techs: ['Angular', 'Python', 'PostgreSQL', 'graphQL']
    },
    {
      name: "Mark'it",
      description: "nomadicdev.portfolio.markit.description",
      play: "https://play.google.com/store/apps/details?id=com.markit.dev",
      github: "https://framagit.org/merodrem/mark-it",
      techs: ['React Native', 'Redux', 'Parse Server']
    },
    {
      name: "2r retailing",
      image: "assets/img/2r.png",
      description: "nomadicdev.portfolio.2r.description",
      url: "https://deux-r.herokuapp.com/login",
      techs: ['Angular', 'Python', 'SOAP', 'graphQL']
    },
    {
      name: "Huntersmash",
      description: "nomadicdev.portfolio.huntersmash.description",
      play: "https://play.google.com/store/apps/details?id=com.huntersmash",
      github: "https://github.com/merodrem/HunterSmash",
      techs: ['React Native']
    },
    {
      name: "This website :-)",
      description: "nomadicdev.portfolio.nomadicdev.description",
      github: "https://framagit.org/merodrem/nomadic-dev",
      techs: ['Angular']
    }
  ];

  EXPERIENCE: Experience[] = [
    {
      company: "KSZ-BCSS",
      techs: ['Java', 'SOAP', 'XSLT'],
      description: "nomadicdev.exp.ksz.description",
      duration: "01.2018 - 01.2019",
    },
    {
      company: "Spacebel",
      techs: ['C++', 'Tcl'],
      description: "nomadicdev.exp.spacebel.description",
      duration: "01.2017 - 08.2017",
    },
    {
      company: "Vidinoti",
      techs: ['Python', 'AngularJS', 'Php', 'MongoDB'],
      description: "nomadicdev.exp.vidinoti.description",
      duration: "09.2015 - 03.2016",
    },
  ];
  constructor(
    private _translate: TranslateService,
    public dialog: MatDialog
  ) { }

  openImageDialog(src: string): void {
    this.dialog.open(ImageDialog, {
      data: { src },
      maxWidth: '100vw',
      width: '100%',
      panelClass: 'full-screen-modal'
    });
  }

  ngOnInit() { }


}
