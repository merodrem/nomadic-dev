import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedModule } from '../shared/shared.module';

import { HomeComponent } from './home.component';
import { HomeRoutingModule } from './home-routing.module';
import { CarouselModule, WavesModule } from 'angular-bootstrap-md'
import { EffectsModule } from '@ngrx/effects';
import { ContactFormComponent } from './contact-form/components/contact-form.component';
import { ContactFormEffects } from './contact-form/contact-form.effects';
@NgModule({
  declarations: [HomeComponent, ContactFormComponent],
  imports: [
    CommonModule, 
    SharedModule, 
    HomeRoutingModule, 
    CarouselModule, WavesModule,
    EffectsModule.forFeature([
      ContactFormEffects
    ])
  ],
})
export class HomeModule {}
