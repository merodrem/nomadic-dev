import { Component, Inject } from "@angular/core";
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';

interface DialogData {
    src: string;
  }
@Component({
    selector: 'nd-image-dialog',
    templateUrl: 'image.dialog.html',
  })
  export class ImageDialog {
    constructor(
      public dialogRef: MatDialogRef<ImageDialog>,
      @Inject(MAT_DIALOG_DATA) public data: DialogData,
    ) {}
  
  }