import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { Store } from '@ngrx/store';
import { actionSettingsToggleNightMode } from '../../core/settings/settings.actions';
import {  State } from '../../core/settings/settings.model';

@Component({
  selector: 'nomadicdev-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SettingsComponent implements OnInit {

  constructor(private store: Store<State>) { }

  ngOnInit(): void {
  }

  toggleNightMode(){
    this.store.dispatch(actionSettingsToggleNightMode());
    
  }

}
