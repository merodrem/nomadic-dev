import { createSelector } from '@ngrx/store';

import { SettingsState } from './settings.model';
import { selectSettingsState } from '../core.state';

export const selectSettings = createSelector(
  selectSettingsState,
  (state: SettingsState) => state
);


export const selectSettingsLanguage = createSelector(
  selectSettings,
  (state: SettingsState) => state.language
);

export const selectTheme = createSelector(
  selectSettings,
  (settings) => settings.theme
);

export const selectPageAnimations = createSelector(
  selectSettings,
  (settings) => settings.pageAnimations
);

export const selectElementsAnimations = createSelector(
  selectSettings,
  (settings) => settings.elementsAnimations
);

export const selectNightTheme = createSelector(
  selectSettings,
  (settings) => settings.nightTheme
);



export const selectIsNightMode = createSelector(
  selectSettings,
  (settings) => settings.isNightMode
);

export const selectEffectiveTheme = createSelector(
  selectTheme,
  selectNightTheme,
  selectIsNightMode,
  (theme, nightTheme, isNightMode) =>
    (isNightMode ? nightTheme : theme).toLowerCase()
);
