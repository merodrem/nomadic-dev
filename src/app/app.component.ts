import browser from 'browser-detect';
import { AfterViewInit, ChangeDetectorRef, Component, HostListener, OnInit, ViewChild } from '@angular/core';
import { MatSelectChange } from '@angular/material/select';
import { Store, select } from '@ngrx/store';
import { Observable, Subscription } from 'rxjs';

import { environment as env } from '../environments/environment';

import {
  routeAnimations,
  LocalStorageService,
  selectSettingsLanguage,
  selectEffectiveTheme,
  AppState
} from './core/core.module';
import {
  actionSettingsChangeAnimationsPageDisabled,
  actionSettingsChangeLanguage
} from './core/settings/settings.actions';
import { MatSidenavContainer } from '@angular/material/sidenav';

@Component({
  selector: 'nomadicdev-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  animations: [routeAnimations]
})
export class AppComponent implements OnInit, AfterViewInit {
  isProd = env.production;
  isTop = true;
  private subscription: Subscription | undefined;
  envName = env.envName;
  version = env.versions.app;
  year = new Date().getFullYear();
  logo = 'assets/img/logo.png';
  languages = ['en', 'fr'];
  navigation = [
    { link: 'about', label: 'nomadicdev.home.title' },
    { link: 'portfolio', label: 'nomadicdev.portfolio.title' },
    { link: 'exp', label: 'nomadicdev.exp.title' },
    { link: 'edu', label: 'nomadicdev.edu.title' },
    { link: 'contact', label: 'nomadicdev.contact.title' },
  ];

  stickyHeader$: Observable<boolean> | undefined;
  language$: Observable<string> | undefined;
  theme$: Observable<string> | undefined;
  @ViewChild(MatSidenavContainer) sidenavContainer: MatSidenavContainer;

  constructor(
    private cdr:ChangeDetectorRef,
    private store: Store<AppState>,
    private storageService: LocalStorageService
  ) { }


  private static isIEorEdgeOrSafari() {
    return ['ie', 'edge', 'safari'].includes(browser().name || '');
  }


  ngAfterViewInit() {
    this.subscription = this.sidenavContainer.scrollable.elementScrolled().subscribe((event) => (
      this.isTop = this.sidenavContainer.scrollable.measureScrollOffset('top') == 0, 
      this.cdr.detectChanges()
      ));
  }

  ngOnInit(): void {
    this.storageService.testLocalStorage();
    if (AppComponent.isIEorEdgeOrSafari()) {
      this.store.dispatch(
        actionSettingsChangeAnimationsPageDisabled({
          pageAnimationsDisabled: true
        })
      );
    }

    this.language$ = this.store.pipe(select(selectSettingsLanguage));
    this.theme$ = this.store.pipe(select(selectEffectiveTheme));
  }

  onLanguageSelect(event: MatSelectChange) {
    this.store.dispatch(
      actionSettingsChangeLanguage({ language: event.value })
    );
  }

  scrollTo(id: string) {
    const itemToScrollTo = document.getElementById(id);
    if (itemToScrollTo) {
      itemToScrollTo.scrollIntoView({ behavior: 'smooth', block: 'center' })

    }
  }

  ngOnDestroy() {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }
}
